﻿namespace CountWords
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCountWords = new System.Windows.Forms.TextBox();
            this.lblCharacters = new System.Windows.Forms.Label();
            this.btnCountChars = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtCountWords
            // 
            this.txtCountWords.Location = new System.Drawing.Point(34, 43);
            this.txtCountWords.Name = "txtCountWords";
            this.txtCountWords.Size = new System.Drawing.Size(201, 20);
            this.txtCountWords.TabIndex = 0;
            // 
            // lblCharacters
            // 
            this.lblCharacters.AutoSize = true;
            this.lblCharacters.Location = new System.Drawing.Point(31, 110);
            this.lblCharacters.Name = "lblCharacters";
            this.lblCharacters.Size = new System.Drawing.Size(182, 13);
            this.lblCharacters.TabIndex = 2;
            this.lblCharacters.Text = "The Number of Characters in a String";
            // 
            // btnCountChars
            // 
            this.btnCountChars.Location = new System.Drawing.Point(34, 69);
            this.btnCountChars.Name = "btnCountChars";
            this.btnCountChars.Size = new System.Drawing.Size(201, 23);
            this.btnCountChars.TabIndex = 3;
            this.btnCountChars.Text = "Count Letters";
            this.btnCountChars.UseVisualStyleBackColor = true;
            this.btnCountChars.Click += new System.EventHandler(this.btnCountChars_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnCountChars);
            this.Controls.Add(this.lblCharacters);
            this.Controls.Add(this.txtCountWords);
            this.Name = "Form1";
            this.Text = "Count Letters in a String";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCountWords;
        private System.Windows.Forms.Label lblCharacters;
        private System.Windows.Forms.Button btnCountChars;
    }
}

