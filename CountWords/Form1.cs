﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CountWords
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCountChars_Click(object sender, EventArgs e)
        {
            /*
             * This program also counts spaces. please use RegularExpressions to eradicate it .
             */
            char[] stringsCount = txtCountWords.Text.ToCharArray();
            string temp = new string(stringsCount);
            int totalNumberOfLetters = temp.Length;
            lblCharacters.Text = "Total Characters : "+ Convert.ToString(totalNumberOfLetters);
        }
    }
}
